import { useState } from "react";

function App() {
  const [winner, setWinner] = useState(1);
  const [firstValue, setFirstValue] = useState(2);
  const [secondValue, setSecondValue] = useState(4);

  function getRandomValue() {
    let value = Math.max(1, (parseInt(Math.random() * 10) + 1) % 7);
    return value;
  }

  function handlePlayClick() {
    let firstValue = getRandomValue();
    let secondValue = getRandomValue();

    if (firstValue > secondValue) {
      setWinner(1);
    } else if (secondValue > firstValue) {
      setWinner(2);
    } else {
      setWinner(null);
    }
    setFirstValue(firstValue);
    setSecondValue(secondValue);
  }

  return (
    <div className="app">
      <div className="header">
        {winner && <h1>Player {winner} wins</h1>}
        {winner == null && <h1>Tie</h1>}
      </div>
      <div className="body">
        <div className="first-section">
          <div className="first-block">
            <h2>Player 1</h2>
            <img src={`./src/assets/dice-face-${firstValue}.png`} alt="" />
          </div>
          <div className="second-block">
            <h2>Player 2</h2>
            <img src={`./src/assets/dice-face-${secondValue}.png`} alt="" />
          </div>
        </div>
        <div className="second-section">
          <button onClick={handlePlayClick}>Play</button>
        </div>
      </div>
    </div>
  );
}

export default App;
